const Sequelize = require('sequelize');
const config = require('../config');
const connectionDataBase = config.connectionDataBase;
const ObjectSequelize = new Sequelize(
  connectionDataBase.dataDase, 
  connectionDataBase.user, 
  connectionDataBase.password, {
    host : connectionDataBase.host,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    operatorsAliases: false
});

ObjectSequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = {
    ObjectSequelize: ObjectSequelize,
    Sequelize: Sequelize
};