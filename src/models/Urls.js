const configDataBase = require('../config/ConfigDataBase');
const Sequelize = configDataBase.Sequelize;
const ObjectSequelize = configDataBase.ObjectSequelize;

const Urls = ObjectSequelize.define('urls', {
    url : {
        type: Sequelize.STRING(500)
    },
    short : {
        type: Sequelize.STRING(255)
    },
},{
    freezeTableName: true,
    timestamps: false,
    underscored: true
});

Urls.removeAttribute('id');

module.exports = Urls;