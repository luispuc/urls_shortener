const express = require('express');
const router = express.Router();
const validUrl = require('valid-url');
const shortId = require('shortid');
const Urls = require('../models/Urls');

router.get('/', function(req, res){
    res.json({
        message: 'Welcome to Short Urls.'
    });
});

router.get('/:short', function(req, res){
    //asgined url short to var short
    const short = req.params.short;

    //find in model Urls
    let url;
    Urls.findOne({where : {short: short}})
        .then(function(urlRes){
            url = urlRes;
            //if exists then redirect, if not, response not found
            if (url){
                res.redirect(url.url);
            } else {
                res.status(404).json({message:'Url not found'});
            }
        });
});

router.post('/generate', async function(req, res){
    //asigned the params to var
    let url = req.body.url;
    const urlBase = 'http://localhost:8081/';//define url base

    try {

        let shortUrls = new Array();//array short urls send by json
        //if is a array generate all short urls, if not, generate only one
        if ( Array.isArray(url) ){
            for ( let urlParam of url ){
                //valid if is a url 
                if (validUrl.isUri(urlParam)){

                    let urlSource;
                    //find if exist
                    urlSource = await Urls.findOne({
                        where:{url: urlParam}
                    });
                        
                    let short;
                    if (urlSource){
                        short = urlBase + urlSource.short;
                        shortUrls.push(short);
                    } else {
                        //if not exist, generate short url
                        let uuid = shortId.generate();
                        short = urlBase + uuid;
                        await Urls.create({
                            url : urlParam,
                            short : uuid
                        });

                        shortUrls.push(short);
                    }
                    

                } else {
                    throw new Error('Some urls is invalid.');
                }
            }

            res.status(200).json({
                shortUrls : shortUrls
            });
        } else {
            //valid if is a url 
            if ( validUrl.isUri(url) ){
                //find if exist
                let element = await Urls.findOne({
                    where : {url: url}
                });

                if (element){
                    let short = urlBase + element.short;
                    res.status(200).json({
                        shortUrl : short
                    });
                } else {
                    //if not exist, generate short url
                    let uuid = shortId.generate();
                    await Urls.create({
                        url : url,
                        short : uuid
                    });
                    
                    let element = await Urls.findOne({
                        where: {url:url}
                    });

                    let short = urlBase + element.short;
                    res.status(200).json({
                        shortUrl : short
                    });
                }
                
            } else {
                throw new Error('Some urls is invalid.');
            }
        }
    } catch(errr){
        res.status(400).json({message: errr.message})
    }
});

module.exports = router;