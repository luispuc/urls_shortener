**Urls Shortener**

Este repositorio es de un API RESTFull el cual acorta urls, la documentaci�n la puede seguir haciendo click [Aqu�](https://documenter.getpostman.com/view/1449301/RWaKToWW). 

# Pasos para correr la API

- Clonar o descargar el repositorio
- Instalar mysql como base de datos
- Instalar [NodeJS](https://nodejs.org/es/)
- Con un software como Workbench, ejecutar el script contenido en la carpeta scripts
- Establecer los parametros de conexi�n a la base de datos en el archivo config.js
- Abrir la consola, dirigirse al repositorio, ejecutar **npm install** y posterior a ello **npm start**
